package com.spring.pipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartPipelineApplication.class, args);
	}

}
